export class Fixtures {
  id: number;
  eventKey: string;
  eventDate: string;
  eventTime: string;
  eventHomeTeam: string;
  eventAwayTeam: string;
  eventFinalResult: string;
  eventStatus: string;
  countryName: string;
  leagueName: string;
  leagueRound: string;
}
