export class Prediction {
  eventKey: string;
  homeTeamScore: number;
  awayTeamScore: number;
}
