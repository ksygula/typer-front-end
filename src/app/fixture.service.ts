import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import {Fixtures} from './interfaces/Fixtures';
import {Prediction} from './interfaces/Prediction';
import {Observable, throwError} from 'rxjs';
import {environment} from '../environments/environment';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FixtureService {

  constructor(private http: HttpClient) { }

  getFixtures() {
    const url = 'http://localhost:8081/api/fixtures';
    return this.http.get<Fixtures[]>(url);
  }
  getFixturesApi() {
    const url = 'http://localhost:8081/allSportsApi/getfixtures';
    return this.http.get(url);
  }
  addPrediction(prediction: Prediction): Observable<Prediction> {
    const url = environment.apis.api + '/prediction/add';
    return this.http.post<Prediction>(url, prediction)
      .pipe(
        catchError(
          err => {console.log(err);
                  return throwError(err);
        })
      );

  }
}
