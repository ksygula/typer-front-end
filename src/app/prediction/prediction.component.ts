import { Component, OnInit } from '@angular/core';
import {Fixtures} from '../interfaces/Fixtures';
import {FixtureService} from '../fixture.service';

@Component({
  selector: 'app-prediction',
  templateUrl: './prediction.component.html',
  styleUrls: ['./prediction.component.css']
})
export class PredictionComponent implements OnInit {
  fixtures: Fixtures[];

  constructor(private fixtureService: FixtureService) { }

  ngOnInit() {
    this.fixtureService.getFixtures().subscribe(
      (fixtures: any[]) => {this.fixtures = fixtures; },
      (error) => console.log(error)
    );
  }
}
