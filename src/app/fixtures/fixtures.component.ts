import { Component, OnInit } from '@angular/core';
import {FixtureService} from '../fixture.service';
import {Fixtures} from '../interfaces/Fixtures';
import {DataSource} from '@angular/cdk/table';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-fixtures',
  templateUrl: './fixtures.component.html',
  styleUrls: ['./fixtures.component.css']
})
export class FixturesComponent implements OnInit {
  dataSource = new FixturesDataSource(this.fixtureService);
  displayedColumns = ['date', 'time', 'homeTeam', 'awayTeam', 'result'];
  fixtures: Fixtures[];
  constructor(private fixtureService: FixtureService) { }

  ngOnInit() {
  }
}
export class FixturesDataSource extends DataSource<any> {
  constructor(private fixtureService: FixtureService) {
    super();
  }
  connect(): Observable<Fixtures[]> {
    return this.fixtureService.getFixtures();
  }
  disconnect() {}
}
