import { CommonModule } from '@angular/common';
import {MatButtonModule, MatToolbarModule, MatCardModule, MatInputModule, MatTableModule} from '@angular/material';
import {NgModule} from '@angular/core';

@NgModule({
  imports: [CommonModule, MatToolbarModule, MatButtonModule, MatCardModule, MatInputModule, MatTableModule],
  exports: [CommonModule, MatToolbarModule, MatButtonModule, MatCardModule, MatInputModule, MatTableModule],
})
export class CustomMaterialModule { }
