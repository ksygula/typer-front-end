import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FixturesComponent} from './fixtures/fixtures.component';
import {AppAuthGuard} from './app.authguard';
import {HomeComponent} from './home/home.component';
import {PredictionComponent} from './prediction/prediction.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'fixtures', component: FixturesComponent, canActivate: [AppAuthGuard]},
  {path: 'prediction', component: PredictionComponent, canActivate: [AppAuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AppAuthGuard]
})
export class AppRoutingModule { }
