import { Component, OnInit } from '@angular/core';
import {KeycloakService} from 'keycloak-angular';
import {AppService} from '../app.service';
import {KeycloakProfile} from 'keycloak-js';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userPoints: number;
  userDetails: KeycloakProfile;

  constructor(private keycloakService: KeycloakService, private service: AppService) {}

  async ngOnInit() {
    if (await this.keycloakService.isLoggedIn()) {
      this.userDetails = await this.keycloakService.loadUserProfile();
      this.userPoints = null;
      this.service.getPoints(this.userDetails.username).subscribe((points: number) => this.userPoints = points);
    }
  }
}
