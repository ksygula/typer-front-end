import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglePredictionComponent } from './single-prediction.component';

describe('SinglePredictionComponent', () => {
  let component: SinglePredictionComponent;
  let fixture: ComponentFixture<SinglePredictionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinglePredictionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglePredictionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
