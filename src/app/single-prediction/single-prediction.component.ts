import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Fixtures} from '../interfaces/Fixtures';
import {Prediction} from '../interfaces/Prediction';
import {FixtureService} from '../fixture.service';

@Component({
  selector: 'app-single-prediction',
  templateUrl: './single-prediction.component.html',
  styleUrls: ['./single-prediction.component.css']
})
export class SinglePredictionComponent implements OnInit {
  predictionForm: FormGroup;
  prediction: Prediction;
  @Input()
  fixture: Fixtures;
  constructor(private formBuilder: FormBuilder, private fixtureService: FixtureService) { }

  ngOnInit() {
    this.predictionForm = this.formBuilder.group({
      homeScore: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*$'),
        Validators.min(0),
        Validators.minLength(1),
        Validators.maxLength(3)
      ])],
      awayScore: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]*$'),
        Validators.min(0),
        Validators.minLength(1),
        Validators.maxLength(3)
      ])]
    });
  }
  onSubmit() {
    this.prediction = new Prediction();
    this.prediction.eventKey = this.fixture.eventKey;
    this.prediction.homeTeamScore = this.predictionForm.get('homeScore').value;
    this.prediction.awayTeamScore = this.predictionForm.get('awayScore').value;
    this.fixtureService.addPrediction(this.prediction).subscribe(
      res => console.log(res),
        error1 => console.log(error1)
    );
  }
}
